package ru.semenov.SOAP;

import javax.jws.WebService;

@WebService(targetNamespace = "https://service.ws.semenov.ru", name = "SoapServiceImpl",serviceName = "SoapService",portName = "SoapServicePort",endpointInterface = "ru.semenov.SOAP.SoapService")
public class SoapServiceImpl implements SoapService {
    @Override
    public String say(String name) {
        return "Приветулики"+name;
    }
}
