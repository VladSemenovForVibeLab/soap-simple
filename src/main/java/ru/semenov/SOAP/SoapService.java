package ru.semenov.SOAP;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

@WebService(targetNamespace = "", name = "SoapService")
public interface SoapService {
    @WebResult(name = "return", targetNamespace = "")
    @WebMethod(action = "urn:Say")
    @ResponseWrapper(localName = "sayResponse", targetNamespace = "https://service.ws.semenov.ru", className = "ru.semenov.SOAP.SayResponse")
    @RequestWrapper(localName = "say", targetNamespace = "https://service.ws.semenov.ru", className = "ru.semenov.SOAP.SoapService")
    String say(@WebParam(name = "name", targetNamespace = "") String name);
}
